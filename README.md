# Django Social Media API
This project contains the APIs required for the proposed social media app.
- Please note that, trailing slash("/") is mandatory for all routes.
- Please add your auth_token in authorization header for the APIs that require login. Example: ```{"Authorization": Token 025d83124c02af9c249b7dbc3a1051234555f107}```

## Account
#### Login
- Request method: POST
- URL: api/account/login/
- Login required: False
- Request:
    ```json
    {
        "username": "user123",
        "password": "password123"
    }
    ```
- Response:
    ```json
    {
        "id": 1,
        "username": "user123",
        "email": "user123@gmail.com",
        "first_name": "",
        "last_name": "",
        "auth_token": "025d83124c02af9c249b7dbc3a1051234555f107"
    }
    ```
    or
    ```json
    {
        "error": "Invalid username or password. Please try again with a valid username or password!"
    }
    ```
  
#### Logout
- Request method: POST
- URL: api/account/logout/
- Login required: True
- Request: None
- Response:
    ```json
    {
        "success": "You have been logged out"
    }
    ```
  
#### Register
- Request method: POST
- URL: api/account/register/
- Login required: False
- Request: 
    ```json
    {
        "username": "user5678",
        "email": "user5678@gmail.com",
        "password": "user5678",
        "first_name": "user",
        "last_name": "5678"
    }
    ```
- Response:
    ```json
    {
        "id": 4,
        "username": "user5678",
        "email": "user5678@gmail.com",
        "first_name": "user",
        "last_name": "5678",
        "auth_token": "9fc79c8f999140f98f5d99eb7d968e416d148b6d"
    }
    ```
    or
    ```json
    {
        "username": [
            "A user with that username already exists."
        ],
        "email": [
            "Enter a valid email address."
        ],
        "password": [
            "This password is too short. It must contain at least 8 characters.",
            "This password is too common."
        ]
    }
    ```
> Registering a user automatically logs him/her in. Use the given token for further operations.

#### Change Password
- Request method: POST
- URL: api/account/change-password/
- Login required: True
- Request: 
    ```json
    {
        "current_password": "current-pass",
        "new_password": "new-pass",
        "new_password_2": "new-pass"
    }
    ```
- Response:
    ```json
    {
        "success": "Password reset successful!"
    }
    ```
    or
    ```json
    {
        "current_password": [
            "This field may not be blank."
        ],
        "new_password": [
            "This field may not be blank."
        ],
        "new_password_2": [
            "This field may not be blank."
        ]
    }
    ```
    or 
    ```json
    {
        "error": "Please provide same password for both new password fields"
    }
    ```
  
#### Forget Password
- Request method: POST
- URL: api/account/reset-password/
- Login required: False
- Request: 
    ```json
    {
        "email": "someone@example.com"
    }
    ```
- Response:
    ```json
    {
        "status": "OK"
    }
    ```
    or
    ```json
    {
        "email": [
            "There is no active user associated with this e-mail address or the password can not be changed"
        ]
    }
  
**Email will be sent to user if the user is registered or email is valid. Then use the token to reset password.**

- Request method: POST
- URL: api/account/reset-password/confirm/
- Login required: False
- Request: 
    ```json
    {
        "token":"0b92384dd8fd5141b9",
        "password":"kaykobad123"
    }
    ```
- Response:
    ```json
    {
        "status": "OK"
    }
    ```
    or
    ```json
    {
        "password": [
            "The password is too similar to the username."
        ]
    }
  
#### My Profile
- Request method: GET
- URL: api/account/my-profile/
- Login required: True
- Request: None
- Response:
    ```json
    {
        "user_id": 1,
        "profile_id": 1,
        "username": "kaykobad",
        "email": "kaykobad@gmail.com",
        "first_name": "",
        "last_name": "",
        "mobile_number": null,
        "bio": null,
        "date_of_birth": null,
        "gender": null,
        "profile_picture": "/media/default_profile_pic.png",
        "points": 100
    }
    ```
    or
    ```json
    {
        "detail": "Authentication credentials were not provided."
    }
  
#### Update Profile
- Request method: POST
- URL: api/account/my-profile/
- Login required: True
- Request:
    ```json
    {
        "first_name": "Kaykobad",
        "last_name": "Reza",
        "mobile_number": "01712345678",
        "bio": "I am a developer",
        "date_of_birth": "YYYY-MM-DD",
        "gender": "male"
    }
    ```
- Response:
    ```json
    {
        "user_id": 1,
        "profile_id": 1,
        "username": "kaykobad",
        "email": "kaykobad@gmail.com",
        "first_name": "Kaykobad",
        "last_name": "Reza",
        "mobile_number": "01712345678",
        "bio": "I am a developer",
        "date_of_birth": "1995-01-01",
        "gender": "male",
        "profile_picture": "/media/profile_picture/685.jpg",
        "points": 100
    }
    ```
    or
    ```json
    {
        "date_of_birth": [
            "Date has wrong format. Use one of these formats instead: YYYY-MM-DD."
        ]
    }  
  
#### Get All Profiles (For Chatting or Friend Request)
- Request method: GET
- URL: api/account/get-all-users/
- Login required: True
- Request: None
- Response:
    ```json
    [
        {
            "user_id": 1,
            "profile_id": 1,
            "username": "kaykobad",
            "email": "kaykobad@gmail.com",
            "first_name": "Kaykobad",
            "last_name": "Reza",
            "mobile_number": "01712345678",
            "bio": "I am a developer",
            "date_of_birth": "1995-01-01",
            "gender": "male",
            "profile_picture": "http://127.0.0.1:8000/media/profile_picture/685.jpg",
            "points": 0
        },
        {
            "user_id": 2,
            "profile_id": 2,
            "username": "reza",
            "email": "",
            "first_name": "",
            "last_name": "",
            "mobile_number": null,
            "bio": null,
            "date_of_birth": null,
            "gender": null,
            "profile_picture": "http://127.0.0.1:8000/media/default_profile_pic.png",
            "points": 0
        },
        {
            "user_id": 3,
            "profile_id": 3,
            "username": "user1234",
            "email": "user1234@gmail.com",
            "first_name": "user",
            "last_name": "1234",
            "mobile_number": null,
            "bio": null,
            "date_of_birth": null,
            "gender": null,
            "profile_picture": "http://127.0.0.1:8000/media/default_profile_pic.png",
            "points": 0
        },
        {
            "user_id": 4,
            "profile_id": 4,
            "username": "user5678",
            "email": "user5678@gmail.com",
            "first_name": "user",
            "last_name": "5678",
            "mobile_number": null,
            "bio": null,
            "date_of_birth": null,
            "gender": null,
            "profile_picture": "http://127.0.0.1:8000/media/default_profile_pic.png",
            "points": 0
        },
        {
            "user_id": 5,
            "profile_id": 5,
            "username": "user0000",
            "email": "user0000@gmail.com",
            "first_name": "Kamal",
            "last_name": "Kadir",
            "mobile_number": "01712345678",
            "bio": "I am a developer",
            "date_of_birth": "1995-01-01",
            "gender": "male",
            "profile_picture": "http://127.0.0.1:8000/media/profile_picture/685_fgnAMrG.jpg",
            "points": 0
        },
        {
            "user_id": 7,
            "profile_id": 6,
            "username": "newuser",
            "email": "newuser@gmail.com",
            "first_name": "new",
            "last_name": "user",
            "mobile_number": null,
            "bio": null,
            "date_of_birth": null,
            "gender": null,
            "profile_picture": "http://127.0.0.1:8000/media/default_profile_pic.png",
            "points": 100
        }
    ]
    ```
    or
    ```json
    {
        "detail": "Authentication credentials were not provided."
    }
    ```
> first_name and last_name is mandatory field when updating profile. If you are updating profile picture, be sure to upload it as multipart-form-data with key "profile_picture".

## News Feed
#### Create Post
- Request method: POST
- URL: api/newsfeed/my-post/create-post/
- Login required: True
- Request:
    ```json
    {
        "post": "This is my first post"
    }
    ```
- Response:
    ```json
    {
        "id": 3,
        "post": "This is my third post",
        "date_created": "2020-10-04T02:25:00.155705Z",
        "is_edited": false,
        "date_edited": null,
        "author_name": "kaykobad",
        "total_comments": 0
    }
    ```
    or
    ```json
    {
        "detail": "Authentication credentials were not provided."
    }
    ```

#### My All Posts
- Request method: GET
- URL: api/newsfeed/my-post/all-posts/
- Login required: True
- Request: None
- Response:
    ```json
    [
        {
            "id": 1,
            "post": "This is my forst post",
            "date_created": "2020-10-04T02:22:35.301597Z",
            "is_edited": false,
            "date_edited": null,
            "author_name": "kaykobad",
            "total_comments": 3
        },
        {
            "id": 2,
            "post": "This is my second post",
            "date_created": "2020-10-04T02:23:03.234941Z",
            "is_edited": false,
            "date_edited": null,
            "author_name": "kaykobad",
            "total_comments": 0
        },
        {
            "id": 3,
            "post": "This is my third post",
            "date_created": "2020-10-04T02:25:00.155705Z",
            "is_edited": false,
            "date_edited": null,
            "author_name": "kaykobad",
            "total_comments": 0
        }
    ]
    ```
    or
    ```json
    {
        "detail": "Authentication credentials were not provided."
    }
    ```

#### Update Post
- Request method: POST
- URL: api/newsfeed/my-post/update-post/<post_id>/
- Login required: True
- Request: 
    ```json
    {
        "post": "My first updated post."
    }
    ```
- Response:
    ```json
    {
        "id": 1,
        "post": "My first updated post.",
        "date_created": "2020-10-04T02:22:35.301597Z",
        "is_edited": true,
        "date_edited": "2020-10-04T03:43:47.154794Z",
        "author_name": "kaykobad",
        "total_comments": 3
    }
    ```
    or
    ```json
    {
        "error": "No post found. Make sure you are the author of the post and try with a valid post id."
    }
    ```

#### Delete Post
- Request method: POST
- URL: api/newsfeed/my-post/delete-post/<post_id>/
- Login required: True
- Request: None
- Response:
    ```json
    {
        "success": "Post deleted from database."
    }
    ```
    or
    ```json
    {
        "error": "No post found. Make sure you are the author of the post and try with a valid post id."
    }
    ```
  
#### Post Details
- Request method: GET
- URL: api/newsfeed/my-post/details/<post_id>/
- Login required: True
- Request: None
- Response:
    ```json
    {
        "id": 1,
        "post": "My first updated post.",
        "date_created": "2020-10-04T02:22:35.301597Z",
        "is_edited": true,
        "date_edited": "2020-10-04T03:43:47.154794Z",
        "author_name": "kaykobad",
        "total_comments": 3,
        "comments": [
            {
                "id": 1,
                "comment": "Noce post bro!",
                "author_name": "user1234",
                "post_id": 1,
                "post_text": "My first updated post.",
                "date_created": "2020-10-04T10:42:27.677170Z",
                "is_edited": false,
                "date_edited": null
            },
            {
                "id": 2,
                "comment": "Thanks bro!",
                "author_name": "kaykobad",
                "post_id": 1,
                "post_text": "My first updated post.",
                "date_created": "2020-10-04T10:53:32.780551Z",
                "is_edited": false,
                "date_edited": null
            },
            {
                "id": 3,
                "comment": "You are welcomed, bro!",
                "author_name": "user1234",
                "post_id": 1,
                "post_text": "My first updated post.",
                "date_created": "2020-10-04T10:53:46.163615Z",
                "is_edited": false,
                "date_edited": null
            }
        ]
    }
    ```
    or
    ```json
    {
        "error": "No post found. Please try with a valid post id."
    }
    ```

#### My News Feed
- Request method: GET
- URL: api/newsfeed/my-feed/
- Login required: True
- Request: None
- Response:
    ```json
    [
        {
            "id": 5,
            "post": "This is my first post as user1234",
            "date_created": "2020-10-04T03:57:42.598081Z",
            "is_edited": false,
            "date_edited": null,
            "author_name": "user1234",
            "total_comments": 0
        },
        {
            "id": 2,
            "post": "My second updated post.",
            "date_created": "2020-10-04T02:23:03.234941Z",
            "is_edited": true,
            "date_edited": "2020-10-04T03:48:56.656215Z",
            "author_name": "kaykobad",
            "total_comments": 0
        },
        {
            "id": 1,
            "post": "My first updated post.",
            "date_created": "2020-10-04T02:22:35.301597Z",
            "is_edited": true,
            "date_edited": "2020-10-04T03:43:47.154794Z",
            "author_name": "kaykobad",
            "total_comments": 3
        }
    ]
    ```
    or
    ```json
    []
    ```
  
#### Create Comment
- Request method: POST
- URL: api/newsfeed/comment/create-comment/
- Login required: True
- Request: 
    ```json
    {
        "post_id": 1,
        "comment": "You are welcomed, bro!"
    }
    ```
- Response:
    ```json
    {
        "id": 3,
        "comment": "You are welcomed, bro!",
        "author_name": "user1234",
        "post_id": 1,
        "post_text": "My first updated post.",
        "date_created": "2020-10-04T10:53:46.163615Z",
        "is_edited": false,
        "date_edited": null
    }
    ```
    or
    ```json
    {
        "error": "Could not comment. Invalid post id or comment format."
    }
    ```    

#### Update Comment
- Request method: POST
- URL: api/newsfeed/comment/update-comment/<comment_id>/
- Login required: True
- Request: 
    ```json
    {
        "comment": "Thanks bro (updated comment)!"
    }
    ```
- Response:
    ```json
    {
        "id": 3,
        "comment": "Thanks bro (updated comment)!",
        "author_name": "user1234",
        "post_id": 1,
        "post_text": "My first updated post.",
        "date_created": "2020-10-04T10:53:46.163615Z",
        "is_edited": true,
        "date_edited": "2020-10-05T02:14:29.960980Z"
    }
    ```
    or
    ```json
    {
        "error": "No comment found. Make sure you are the author of the comment."
    }
    ```  

#### Delete Comment
- Request method: POST
- URL: api/newsfeed/comment/delete-comment/<comment_id>
- Login required: True
- Request: None
- Response:
    ```json
    {
        "success": "Comment deleted from database."
    }
    ```
    or
    ```json
    {
        "error": "No comment found. Make sure you are the author of the comment and try with a valid comment id."
    }
    ```

## Chat
#### Send SMS
- Request method: POST
- URL: api/chat/send-message/
- Login required: True
- Request: 
    ```json
    {
        "receiver": 7,
        "message": "Hello bro"
    }
    ```
- Response:
    ```json
    [
        {
            "id": 1,
            "message": "Hello bro",
            "sender": "7",
            "receiver": 1,
            "send_date": "2020-10-07T03:27:58.693045Z",
            "thread_id": 1
        },
        {
            "id": 2,
            "message": "Hello bro, please reply bro",
            "sender": "7",
            "receiver": 1,
            "send_date": "2020-10-07T03:28:12.746444Z",
            "thread_id": 1
        }
    ]
    ```
    or
    ```json
    {
        "error": "You can not sen message to yourself!"
    }
    ```

#### All Threads
- Request method: GET
- URL: api/chat/all-threads/
- Login required: True
- Request: None
- Response:
    ```json
    [
        {
            "id": 1,
            "first_participant": "newuser",
            "first_participant_id": "7",
            "second_participant": "kaykobad",
            "second_participant_id": "1",
            "date_created": "2020-10-07T03:27:58.690220Z",
            "date_updated": "2020-10-07T03:27:58.690229Z",
            "unseen_message_for_participant_1": false,
            "unseen_message_for_participant_2": false
        },
        {
            "id": 2,
            "first_participant": "newuser",
            "first_participant_id": "7",
            "second_participant": "reza",
            "second_participant_id": "2",
            "date_created": "2020-10-07T03:33:12.894408Z",
            "date_updated": "2020-10-07T03:33:12.894416Z",
            "unseen_message_for_participant_1": false,
            "unseen_message_for_participant_2": false
        },
        {
            "id": 3,
            "first_participant": "newuser",
            "first_participant_id": "7",
            "second_participant": "user5678",
            "second_participant_id": "4",
            "date_created": "2020-10-07T03:33:41.039747Z",
            "date_updated": "2020-10-07T03:33:41.039764Z",
            "unseen_message_for_participant_1": false,
            "unseen_message_for_participant_2": false
        }
    ]
    ```
    or
    ```json
    []
    ```
  
#### All Messages of a thread
- Request method: GET
- URL: api/chat/thread/<thread_id>
- Login required: True
- Request: None
- Response:
    ```json
    [
        {
            "id": 1,
            "message": "Hello bro",
            "sender": 7,
            "receiver": 1,
            "send_date": "2020-10-07T03:27:58.693045Z",
            "thread_id": 1
        },
        {
            "id": 2,
            "message": "Hello bro, please reply bro",
            "sender": 7,
            "receiver": 1,
            "send_date": "2020-10-07T03:28:12.746444Z",
            "thread_id": 1
        },
        {
            "id": 6,
            "message": "Hello bro, please reply bro",
            "sender": 7,
            "receiver": 1,
            "send_date": "2020-10-07T04:41:14.307913Z",
            "thread_id": 1
        }
    ]
    ```
    or
    ```json
    {
        "error": "No thread found."
    }
    ```