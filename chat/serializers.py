from rest_framework import serializers
from .models import Thread, Message


class ThreadSerializer(serializers.ModelSerializer):
    first_participant_id = serializers.CharField(source='participant_1.id')
    first_participant = serializers.CharField(source='participant_1.username')
    second_participant_id = serializers.CharField(source='participant_2.id')
    second_participant = serializers.CharField(source='participant_2.username')

    class Meta:
        model = Thread
        fields = ('id', 'first_participant', 'first_participant_id', 'second_participant', 'second_participant_id', 'date_created', 'date_updated', 'unseen_message_for_participant_1', 'unseen_message_for_participant_2')
        read_only_fields = ('id', 'first_participant', 'first_participant_id', 'second_participant', 'second_participant_id', 'date_created', 'date_updated', 'unseen_message_for_participant_1', 'unseen_message_for_participant_2')


class MessageSerializer(serializers.ModelSerializer):
    sender = serializers.IntegerField(source='sender.id', required=False)
    receiver = serializers.IntegerField(source='receiver.id', required=True)
    thread_id = serializers.IntegerField(source='thread.id', required=False)

    class Meta:
        model = Message
        fields = ('id', 'message', 'sender', 'receiver', 'send_date', 'thread_id')
        read_only_fields = ('id', 'sender', 'send_date', 'thread_id')
