from django.core.exceptions import ObjectDoesNotExist
from .models import Thread
from django.db.models import Q


def get_thread(user1, user2):
    try:
        thread = Thread.objects.get(Q(participant_1=user1, participant_2=user2) | Q(participant_2=user1, participant_1=user2))
    except ObjectDoesNotExist:
        thread = Thread.objects.create(participant_1=user1, participant_2=user2)
    return thread


def get_all_threads(user):
    try:
        threads = Thread.objects.filter(Q(participant_1=user) | Q(participant_2=user))
    except ObjectDoesNotExist:
        threads = None
    return threads
