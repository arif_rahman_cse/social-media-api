from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Thread(models.Model):
    participant_1 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='participant_1')
    participant_2 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='participant_2')
    date_created = models.DateTimeField(editable=False)
    date_updated = models.DateTimeField(null=True, blank=True)
    unseen_message_for_participant_1 = models.BooleanField(default=True)
    unseen_message_for_participant_2 = models.BooleanField(default=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.date_created = timezone.now()
            self.date_updated = timezone.now()
        return super(Thread, self).save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return self.participant_1.username + " - " + self.participant_2.username


class Message(models.Model):
    message = models.TextField()
    send_date = models.DateTimeField(editable=False)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender')
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='receiver')
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE, related_name='thread')

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.send_date = timezone.now()
        self.thread.date_updated = timezone.now()
        return super(Message, self).save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return self.message + ': ' + self.sender.username + " -> " + self.receiver.username
