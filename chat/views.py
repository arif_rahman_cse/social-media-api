from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, status
from django.contrib.auth.models import User
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

from .models import Thread, Message
from .serializers import ThreadSerializer, MessageSerializer
from .utils import get_thread, get_all_threads


class ChatViewSet(viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated, ]

    @action(methods=['POST', ], detail=False, url_path='send-message')
    def send_message(self, request):
        serializer = MessageSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            receiver = User.objects.get(id=serializer.validated_data['receiver']['id'])
        except ObjectDoesNotExist:
            data = {"error": "Receiver does not exists!"}
            return Response(data=data, status=status.HTTP_400_BAD_REQUEST)

        if receiver.id == request.user.id:
            data = {"error": "You can not sen message to yourself!"}
            return Response(data=data, status=status.HTTP_403_FORBIDDEN)

        thread = get_thread(request.user, receiver)
        try:
            message = Message.objects.create(
                message=serializer.validated_data['message'],
                sender=request.user,
                receiver=receiver,
                thread=thread
            )
            messages = Message.objects.filter(thread=thread)

            if thread.participant_1 == request.user:
                thread.unseen_message_for_participant_1 = False
                thread.unseen_message_for_participant_2 = True
            else:
                thread.unseen_message_for_participant_1 = True
                thread.unseen_message_for_participant_2 = False
            thread.save()
            
            serialized_messages = MessageSerializer(messages, many=True)
            return Response(data=serialized_messages.data, status=status.HTTP_200_OK)
        except:
            data = {"error": "Could not send message."}
            return Response(data=data, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['GET', ], detail=False, url_path='all-threads')
    def all_threads(self, request):
        threads = get_all_threads(request.user)
        if threads is None:
            data = {"message": "No thread found."}
            return Response(data=data, status=status.HTTP_404_NOT_FOUND)
        serializer = ThreadSerializer(threads, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(methods=['GET', ], detail=False, url_path='thread/(?P<thread_id>[^/.]+)')
    def all_messages(self, request, thread_id):
        try:
            thread = Thread.objects.get(Q(id=thread_id, participant_1=request.user) | Q(id=thread_id, participant_2=request.user))
            messages = Message.objects.filter(thread=thread)

            if thread.participant_1 == request.user:
                thread.unseen_message_for_participant_1 = False
            else:
                thread.unseen_message_for_participant_2 = False
            thread.save()

            serializer = MessageSerializer(messages, many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            data = {"error": "No thread found."}
            return Response(data=data, status=status.HTTP_404_NOT_FOUND)
