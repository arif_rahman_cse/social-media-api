from rest_framework import routers
from django.urls import path, include
from .views import AuthViewSet, UpdateProfileView, AllProfilesView

router = routers.DefaultRouter()
router.register('', AuthViewSet, basename='account')

urlpatterns = [
    path('my-profile/', UpdateProfileView.as_view(), name='my-profile'),
    path('reset-password/', include('django_rest_passwordreset.urls', namespace='password_reset')),
    path('get-all-users/', AllProfilesView.as_view(), name='all-profiles'),
]
urlpatterns += router.urls
