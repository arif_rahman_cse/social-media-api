from rest_framework import serializers
from .models import Post, Comment


class PostSerializer(serializers.ModelSerializer):
    author_name = serializers.CharField(source='author.username', required=False)
    total_comments = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ('id', 'post', 'date_created', 'is_edited', 'date_edited', 'author_name', 'total_comments')
        read_only_fields = ('id', 'date_created', 'is_edited', 'date_edited', 'author_name', 'total_comments')

    def get_total_comments(self, obj):
        return obj.comments.count()


class CommentSerializer(serializers.ModelSerializer):
    author_name = serializers.CharField(source='author.username', read_only=True)
    post_id = serializers.IntegerField(source='post.id', required=False)
    post_text = serializers.CharField(source='post.post', read_only=True)

    class Meta:
        model = Comment
        fields = ('id', 'comment', 'author_name', 'post_id', 'post_text', 'date_created', 'is_edited', 'date_edited')
        read_only_fields = ('id', 'author_name', 'post_text', 'date_created', 'is_edited', 'date_edited')
