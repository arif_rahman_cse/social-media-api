from rest_framework import routers
from django.urls import path, include
from .views import NewsFeedViewSet, CommentViewSet

router = routers.DefaultRouter()
router.register('', NewsFeedViewSet, basename='newsfeed')
router.register('comment', CommentViewSet, basename='comment')

urlpatterns = router.urls
