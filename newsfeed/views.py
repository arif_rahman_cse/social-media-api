from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, status
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist

from .models import Post, Comment
from .serializers import PostSerializer, CommentSerializer


class NewsFeedViewSet(viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated, ]
    serializer_class = PostSerializer

    @action(methods=['GET', ], detail=False, url_path='my-feed')
    def my_news_feed(self, request):
        posts = Post.objects.all().order_by('-date_created')
        serializer = PostSerializer(posts, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(methods=['POST', ], detail=False, url_path='my-post/create-post')
    def create_post(self, request):
        serializer = PostSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        post = Post.objects.create(
            author=request.user,
            post=serializer.validated_data['post'],
        )
        data = PostSerializer(post).data
        return Response(data=data, status=status.HTTP_201_CREATED)

    @action(methods=['GET', ], detail=False, url_path='my-post/all-posts')
    def all_posts(self, request):
        posts = Post.objects.filter(author=request.user).order_by('-date_created')
        serializer = PostSerializer(posts, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(methods=['POST', ], detail=False, url_path='my-post/delete-post/(?P<post_id>[^/.]+)')
    def delete_post(self, request, post_id):
        try:
            post = Post.objects.get(id=post_id, author=request.user)
            post.delete()
            data = {"success": "Post deleted from database."}
            return Response(data=data, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            data = {"error": "No post found. Make sure you are the author of the post and try with a valid post id."}
            return Response(data=data, status=status.HTTP_403_FORBIDDEN)

    @action(methods=['POST', ], detail=False, url_path='my-post/update-post/(?P<post_id>[^/.]+)')
    def update_post(self, request, post_id):
        try:
            post = Post.objects.get(id=post_id, author=request.user)
            serializer = PostSerializer(data=request.data)
            if serializer.is_valid():
                post.post = serializer.validated_data['post']
                post.save()
                serialized_post = PostSerializer(post)
                return Response(data=serialized_post.data, status=status.HTTP_200_OK)
            else:
                return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except ObjectDoesNotExist:
            data = {"error": "No post found. Make sure you are the author of the post and try with a valid post id."}
            return Response(data=data, status=status.HTTP_403_FORBIDDEN)

    @action(methods=['GET', ], detail=False, url_path='my-post/details/(?P<post_id>[^/.]+)')
    def post_details(self, request, post_id):
        try:
            post = Post.objects.get(id=post_id)
            comments = post.comments.all()
            serialized_post = PostSerializer(post)
            serialized_comments = CommentSerializer(comments, many=True)
            data = serialized_post.data
            data['total_comments'] = comments.count()
            data['comments'] = serialized_comments.data
            return Response(data=data, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            data = {"error": "No post found. Please try with a valid post id."}
            return Response(data=data, status=status.HTTP_400_BAD_REQUEST)


class CommentViewSet(viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated, ]
    serializer_class = CommentSerializer

    @action(methods=['POST', ], detail=False, url_path='create-comment')
    def create_comment(self, request):
        serializer = CommentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            post = Post.objects.get(id=serializer.validated_data.pop('post')['id'])
            comment = Comment.objects.create(
                author=request.user,
                post=post,
                comment=serializer.validated_data['comment'],
            )
            data = CommentSerializer(comment).data
            return Response(data=data, status=status.HTTP_201_CREATED)
        except ObjectDoesNotExist:
            data = {"error": "Could not comment. Invalid post id or comment format."}
            return Response(data=data, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['POST', ], detail=False, url_path='delete-comment/(?P<comment_id>[^/.]+)')
    def delete_comment(self, request, comment_id):
        try:
            comment = Comment.objects.get(id=comment_id, author=request.user)
            comment.delete()
            data = {"success": "Comment deleted from database."}
            return Response(data=data, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            data = {"error": "No comment found. Make sure you are the author of the comment and try with a valid comment id."}
            return Response(data=data, status=status.HTTP_403_FORBIDDEN)

    @action(methods=['POST', ], detail=False, url_path='update-comment/(?P<comment_id>[^/.]+)')
    def update_comment(self, request, comment_id):
        try:
            comment = Comment.objects.get(id=comment_id, author=request.user)
            serializer = CommentSerializer(data=request.data)
            if serializer.is_valid():
                comment.comment = serializer.validated_data['comment']
                comment.save()
                serialized_comment = CommentSerializer(comment)
                return Response(data=serialized_comment.data, status=status.HTTP_200_OK)
            else:
                return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except ObjectDoesNotExist:
            data = {"error": "No comment found. Make sure you are the author of the comment."}
            return Response(data=data, status=status.HTTP_403_FORBIDDEN)
